import { Component, OnInit, OnDestroy } from '@angular/core';
import { MENU_ITEMS } from './admin-menu';
import * as $ from 'jquery';


@Component({
  selector: 'ng-admin',
  template: `
    <div class="ng-admin" [ngClass]="{'admin-fixed': fixed, 'admin-static':!fixed}">
      <admin-header></admin-header> 
      <div class="app-mn-cntn">
        <app-menu></app-menu>
        <div class="container-fluid app-cntent">
          <admin-breadcrumbs></admin-breadcrumbs>
          <div class="admin-content">
            <router-outlet></router-outlet>
          </div> 
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./general.scss']
})
export class AdminComponent  implements OnInit{

  menu = MENU_ITEMS;
  fixed=true;

  constructor() {
   }

   ngOnInit() {
      $('body').addClass('admin-bg');
    }

    ngOnDestroy(){
      $('body').removeClass('admin-bg');
    }
 
}
