export const menu=[
  {
    title:"home",
    path:"admin/dashboard",
    icon:"assets/images/littlehijabi.png",
  },
  {
    title:"dashboard",
    icon:"assets/images/littlehijabi.png",
    child:[
      {
        title:"my associates",
        path:"admin/associates",
      },
      {
        title:"claims",
        path:"admin/claims",
      },
      {
        title:"leave",
        path:"admin/leave",
      },
      {
        title:"timesheet",
        path:"admin/timesheet",
      },
      {
        title:"payslip",
        path:"admin/payslip",
      }
    ]
  },
  {
    title:"Tasks",
    icon:"assets/images/littlehijabi.png",
    child:[
      {
        title:"ADD NEW",
      },
      {
        title:"client",
        path:"admin/task/client",
      },
      {
        title:"job order",
        path:"admin/job-order",
      },
      {
        title:"hirring proposal",
        path:"admin/hiring-proposal",
      },
      {
        title:"employment Contract",
        path:"admin/employment-contract",
      },
      {
        title:"associate",
        path:"admin/associate",
      },

      {
        title:"configure",
      },
      {
        title:"claim",
        path:"admin/claims",
      },
      {
        title:"leave",
        path:"admin/leave",
      },
      {
        title:"timesheet",
        path:"admin/timesheet",
      },
      {
        title:"payslip",
        path:"admin/payslip",
      },
      {
        title:"roles & authorisation",
        path:"admin/roles-authorisation",
      }
    ]
  },
  
  {
    title:"blockchain console",
    path:"admin/blockchain-console",
    // iconclass:"fa fa-gear",
    icon:"assets/images/littlehijabi.png"
  }
];