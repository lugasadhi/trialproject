import { Component, OnInit } from '@angular/core';
import {menu} from './menu';
import * as $ from 'jquery';
import {Router} from '@angular/router';


@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  mn;
  menuClick=[];

  constructor(private route:Router) {
  }
  
  ngOnInit() {
    this.mn = menu;
    this.firstActiveMenu();
  }
  
  firstActiveMenu(){
    let ss = menu;
    
    for (let i = 0; i < ss.length; i++) {
      let scs=false;
      if(ss[i].child != undefined){
        for (let ii = 0; ii < ss[i].child.length; ii++) {
          if("/"+ss[i].child[ii].path === this.route.url){
            scs = true;
            break;
          }
        }
        if(scs){
          this.menuClick.push(true);
          break;
        }else{
          this.menuClick.push(false);
        }
      }else{
        this.menuClick.push(false);
      }
    }
  }


}
