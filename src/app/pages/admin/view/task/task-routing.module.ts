import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { TaskComponent } from './task.component';
import { ClientComponent } from './client/client.component';
import { AddClientComponent } from './client/add-client/add-client.component';



const routes: Routes = [
 
  {
    path: '',
    component: TaskComponent,
    children: [{
        path: 'client',
        component: ClientComponent,
      },
      {
        path: 'client/add-new-client',
        component: AddClientComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  // providers:[AuthGuard]
})
export class TaskRoutingModule {
}
