import { Component, OnInit, ViewChild } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatHorizontalStepper} from '@angular/material/stepper';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';

@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.component.html',
  styleUrls: ['./add-client.component.scss']
})
export class AddClientComponent implements OnInit {
  isLinear = true;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;

  constructor(
    private _formBuilder: FormBuilder
  ) { }

  @ViewChild(MatHorizontalStepper) stepper: MatHorizontalStepper;
  
  complete(){
    this.stepper.selected.completed = true;
    this.stepper.selected.editable = true;
    this.stepper.next();
  }


  // set users
  displayedColumns = ['name', 'role', 'contract_no', 'email'];
  dataSource = new MatTableDataSource<clientStatus>(client);

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngOnInit() {

    //enter company details
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });

    //set users
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    //configure master templates


  }

}





export interface clientStatus {
  name: string;
  role: string;
  contract_no: string;
  email: string;
}

const client: clientStatus[] = [
  { name: 'Mary Tan', role: "HR Manager", contract_no: '+65-90001234', email:"mary@ibm.com"},
  { name: 'John Lim', role: "Asst. HR Manager", contract_no: '+65-90001234', email:"john@ibm.com"},
];