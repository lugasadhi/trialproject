import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';


@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss']
})
export class ClientComponent implements OnInit {

  constructor() { }

  displayedColumns = ['id', 'name', 'status', 'created_by'];
  dataSource = new MatTableDataSource<clientStatus>(this.elementChange());

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  elementChange(){
    for (let i = 0; i < client.length; i++) {
      if(client[i].id == ""){
        client[i].id = "-";
      }
      if(client[i].name == ""){
        client[i].name = "-";
      }
      if(client[i].status == ""){
        client[i].status = "-";
      }
      if(client[i].created_by == ""){
        client[i].created_by = "-";
      }
    }
    return client;
  }

}


export interface clientStatus {
  id: string;
  name: string;
  status: string;
  created_by: string;
}

const client: clientStatus[] = [
  {id: "", name: 'IBM Singapore Pte Ltd', status: "draft", created_by: 'Admin_Mary Tan_29 Jan'},
  {id: "0001", name: 'IBM Singapore Pte Ltd', status: "complete", created_by: ''},
];