import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// material
import {
  MatInputModule,
  MatTableModule,
  MatSortModule,
  MatPaginatorModule,
  MatStepperModule,
  MatRadioModule,
  MatIconModule,
  MatSelectModule,
  MatCheckboxModule,
} from '@angular/material';

import { TaskRoutingModule } from './task-routing.module';
import { TaskComponent } from './task.component';
import { ClientComponent } from './client/client.component';
import { AddClientComponent } from './client/add-client/add-client.component';


const ADMIN_COMPONENTS = [
];

@NgModule({
  imports: [
    TaskRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    // material
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatStepperModule,
    MatRadioModule,
    MatIconModule,
    MatSelectModule,
    MatCheckboxModule,
  ],
  declarations: [
    ...ADMIN_COMPONENTS,
    TaskComponent,
    ClientComponent,
    AddClientComponent,
  ],
  exports: [],

})
export class TaskModule {
}
