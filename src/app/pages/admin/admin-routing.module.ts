import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { AdminComponent } from './admin.component';

import { DashboardComponent } from './view/dashboard/dashboard.component';
// import { ClientComponent } from './view/task/client/client.component';



const routes: Routes = [
 
  {
    path: '',
    component: AdminComponent,
    children: [{
        path: 'dashboard',
        component: DashboardComponent,
      },
      { path: 'task', loadChildren: './view/task/task.module#TaskModule'},


      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
      }, 
      
    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  // providers:[AuthGuard]
})
export class AdminRoutingModule {
}
