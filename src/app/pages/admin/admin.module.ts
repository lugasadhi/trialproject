import { NgModule } from '@angular/core';

// material
import {MatInputModule} from '@angular/material';

import { AdminComponent } from './admin.component';
import { AdminRoutingModule } from './admin-routing.module';

import { DashboardModule } from './view/dashboard/dashboard.module';
import { MenuComponent } from './menu/menu.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { BreadcrumbsComponent } from './breadcrumbs/breadcrumbs.component';
import { CommonModule } from '@angular/common';
// import { ClientComponent } from './view/task/client/client.component';



const ADMIN_COMPONENTS = [
  AdminComponent,
];

@NgModule({
  imports: [
    AdminRoutingModule,
    DashboardModule,
    CommonModule,

    // material
    MatInputModule,
  ],
  declarations: [
    ...ADMIN_COMPONENTS,
    MenuComponent,
    FooterComponent,
    HeaderComponent,
    BreadcrumbsComponent,
    // ClientComponent,
  ],
  exports: [],

})
export class AdminModule {
}
