import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, NavigationEnd} from '@angular/router';
import {menu} from '../menu/menu';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'admin-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.scss']
})
export class BreadcrumbsComponent implements OnInit {
  breadCrumbs=[];
  title;

  constructor(
    private route:Router,
    private activatedRoute: ActivatedRoute,
  ) { 
    route.events.pipe(
      filter(event => event instanceof NavigationEnd)
    ).subscribe(() => {
      this.breadCrumbs = undefined;
      this.breadCrumbs = [];
      this.chngSub();
      this.firstActiveMenu();
    });
  }

  ngOnInit() {
    this.breadCrumbs = undefined;
    this.breadCrumbs = [];
    this.chngSub();
    this.firstActiveMenu();
  }

  chngSub(){
    let url = this.route.url;
    let sls = url.split("/");
    for (let index = 2; index < sls.length; index++) {
      let ds = sls[index].replace(/-/g, " ");
      this.breadCrumbs.push(ds);
    }
    this.title = sls[sls.length-1].replace(/-/g, " ");
  }


  firstActiveMenu(){
    let ss = menu;
    let iki = 0;
    for (let i = 0; i < ss.length; i++) {
      let scs=false;
      if(ss[i].child != undefined){
        for (let ii = 0; ii < ss[i].child.length; ii++) {
          if("/"+ss[i].child[ii].path === this.route.url){
            scs = true;
            break;
          }
        }
        if(scs){
          this.breadCrumbs[0]=ss[i].title;
          break;
        }
      }
    }
  }

  
}
