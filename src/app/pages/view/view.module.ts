import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViewComponent } from './view.component';
import { ViewRoutingModule } from './view-routing.module';

import { MenuComponent } from './menu/menu.component';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './footer/footer.component';


const PAGES_COMPONENTS = [
];

@NgModule({
  imports: [
    ViewRoutingModule,
    CommonModule,
  ],
  declarations: [
    MenuComponent,
    ViewComponent,
    FooterComponent,
    HomeComponent,
    ...PAGES_COMPONENTS,
  ],
  exports: [],

})
export class ViewModule {
}
