import { Component, OnInit } from '@angular/core';
import {FormControl, Validators, FormGroup} from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';

import {AuthService} from '../../../services/auth/auth.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: [
    './login.component.scss',
    '../general.scss'
  ]
})


export class LoginComponent implements OnInit {
  
  
  constructor(
    private authService: AuthService  
  ) { }
  
  usernameNotValid = false;
  passwordNotValid = false;
  value = '';
  loginForm : FormGroup;
  showPass = false;
  
  ngOnInit() {
    this.loginForm = new FormGroup({
      username : new FormControl('', [
        Validators.required,
      ]),
      password : new FormControl('', [
        Validators.required,
      ])
    })
  }

  deleteUserInvalidErr(event: any){
    this.usernameNotValid = false;
  }

  deletePassInvalidErr(event: any){
    this.passwordNotValid = false;
  }
  
  
  onSubmit(){
    if(this.loginForm.valid){
      this.authService.login(this.loginForm.value.username, this.loginForm.value.password)
      .then(res => {
        console.log(res);
      }, err => {
        console.log(err.code);
        if(err.code == 'auth/user-not-found'){
          this.usernameNotValid = true;
        }

        if(err.code == 'auth/wrong-password'){
          this.passwordNotValid = true;
        }
      });
    }
  }

}
