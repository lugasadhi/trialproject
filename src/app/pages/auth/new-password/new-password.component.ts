import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-new-password',
  templateUrl: './new-password.component.html',
  styleUrls: ['./new-password.component.scss','../general.scss']
})
export class NewPasswordComponent implements OnInit {

  constructor() { }

  formActive=true;

  ngOnInit() {
  }


  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  telFormControl = new FormControl('', [
    Validators.required,
  ]);

  
  //button function
  passwordChange(){
    this.formActive = false;
  }
}
