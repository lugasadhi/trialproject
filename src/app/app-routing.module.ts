import { NgModule } from '@angular/core';
import { ExtraOptions, Routes, RouterModule } from '@angular/router';
import { P404Component } from './pages/pagenotfound/p404.component';

const routes: Routes = [
  { path: 'admin', loadChildren: './pages/admin/admin.module#AdminModule'},
  { path: 'view', loadChildren: './pages/view/view.module#ViewModule'},
  { path: 'auth', loadChildren: './pages/auth/auth.module#AuthModule'},
  { path: '', redirectTo: 'auth', pathMatch: 'full' },
  { path: '**', component: P404Component },
];

const config: ExtraOptions = {
  useHash: false,
};

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
